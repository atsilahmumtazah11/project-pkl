@extends('layouts.konsultasi.app')

@section('container')
<div class="container">
    <div class="signup-content">
        <div class="signup-form">
            <h2 class="form-title">Berhasil mendaftar konsultasi!</h2>
            {{-- <h3 class="mb-4">Status Antrian : {{ $status_transaction }}</h3> --}}
            <h3 class="mb-4">Nomor Antrian Anda : {{ $konsul->no_antrian }}</h3>
            <h3>Nama : {{ $konsul->nama }}</h3>
            <h3>Asal Instansi : {{ $konsul->asal_instansi }}</h3>
            <h3>Jenis Konsultasi : {{ $konsul->jenis_konsultasi }}</h3>
            <div id="reviewSection" style="display: none;">
                <form method="POST" action="{{ route('savereview') }}" class="register-form">
                    <div class="form-group">
                        <textarea id="reviewTextArea" rows="2" placeholder="Masukkan review Anda di sini..."></textarea><br>
                    <!-- Tambahkan input untuk rating di sini -->
                        <input type="number" id="ratingInput" min="1" max="5" placeholder="Rating (1-5)">
                        <button class="form-submit" onclick="submitReview()">Submit Review</button>
                    </div>
                </form>
            </div>
            <div id="reviewLink">
                <a href="#" onclick="showReviewForm()">Berikan review Anda disini!</a>
            </div>
        </div>
        <div class="signup-image">
            <figure><img src="images/success.jpg"></figure>
        </div>
    </div>
</div>
<script>
    function showReviewForm() {
        document.getElementById("reviewSection").style.display = "block";
        document.getElementById("reviewLink").style.display = "none";
    }

    function submitReview() {
        // Lakukan tindakan untuk mengirim review, misalnya melalui AJAX ke server
        var review = document.getElementById("reviewTextArea").value;
        var rating = document.getElementById("ratingInput").value;
        console.log("Review: " + review);
        console.log("Rating: " + rating);
        // Jika Anda ingin melakukan tindakan lain, tambahkan di sini
    }

    // Kirim data review dan rating ke endpoint save-review menggunakan AJAX
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/save-review", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText); // Respon dari server
            // Tindakan jika review berhasil disimpan
        }
    };
    var data = JSON.stringify({ "review": review, "rating": rating });
    xhr.send(data);
</script>
@endsection
