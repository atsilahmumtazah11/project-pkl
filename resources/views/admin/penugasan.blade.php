@extends('layouts.admin.app')

@section('container')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @if(Session::has('confirm_delete'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('confirm_delete') }}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        <p class="card-title">Daftar Konsultasi</p>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table id="penugasan" class="display expandable-table table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nomor Antrian</th>
                                                <th>Nama Pemrakarsa</th>
                                                <th>Asal Instansi</th>
                                                <th>Jenis Konsultasi</th>
                                                <th>Status</th>
                                                <th>Jam Konsultasi</th>
                                                <th>Assigned</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($konsul as $data)
                                                <tr>
                                                    <td>{{ $data->no_antrian }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->asal_instansi }}</td>
                                                    <td>{{ $data->jenis_konsultasi }}</td>
                                                    <td>
                                                        @if ($data->status_transaction == 'Diterima')
                                                            <label class="badge badge-warning">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Ditolak')
                                                            <label class="badge badge-danger">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Selesai')
                                                            <label class="badge badge-success">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Menunggu')
                                                            <label class="badge badge-secondary">{{ $data->status_transaction }}</label>
                                                        @endif
                                                    </td>
                                                    <td>{{ $data->created_at }}</td>
                                                    <td>
                                                        {{ $data->assigned_pjm }}
                                                        {{-- @foreach ($daftarPJM as $pjm)
                                                            @if ($pjm->id_pjm == $data->id_pjm)
                                                                {{ $pjm->nama_pjm }}
                                                            @endif
                                                        @endforeach --}}
                                                    </td>
                                                    <td>

                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#inputModal{{ $data->id }}" {{ $data->assigned_pjm ? 'disabled' : '' }}>
                                                              <i class="mdi mdi-plus"></i>
                                                            </button>
                                                            <button href="{{ url('edit.assign', $data->id) }}" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editModal{{ $data->id }}" {{ $data->status_transaction == "Selesai" || $data->status_transaction == "Diterima" ? 'disabled' : ''  }}>
                                                              <i class="mdi mdi-pencil"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#deleteModal{{ $data->id }}">
                                                              <i class="mdi mdi-delete"></i>
                                                            </button>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                    </table>



                                            <!-- Modal Inputan -->
                                            @foreach ($konsul as $data)
                                            <div class="modal fade" id="inputModal{{ $data->id }}" tabindex="-1" aria-labelledby="inputModalLabel{{ $data->id }}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="inputModalLabel{{ $data->id }}">Assign <span class="btn btn-info btn-rounded btn-fw btn-sm">No. Antrian : {{ $data->no_antrian }}</span></h5>

                                                            {{-- <button type="button" data-bs-dismiss="modal"><i class="mdi mdi-close-circle"></i></button> --}}
                                                            <button type="button" data-bs-dismiss="modal" class="btn btn-outline-secondary btn-rounded btn-icon">
                                                                <i class="mdi mdi-close"></i>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Form inputan bisa dimasukkan di sini -->
                                                            <form method="POST" action="{{ route('simpan.assign', ['id' => $data->id]) }}">
                                                                @csrf
                                                                <div class="mb-3">
                                                                    <label>Informasi Pemrakarsa :</label>
                                                                    <blockquote class="blockquote">
                                                                        <h6>Nama : {{ $data->nama }}</h6>
                                                                        <h6>Asal Instansi : {{ $data->asal_instansi }}</h6>
                                                                        <h6>No. HP : {{ $data->no_hp }}</h6>
                                                                        <h6>Jenis Konsultasi : {{ $data->jenis_konsultasi }}</h6>
                                                                        <h6>Deskripsi Keluhan : {{ $data->desc_keluhan }}</h6>
                                                                        <h6>Waktu Konsultasi : {{ $data->created_at }}</h6>
                                                                        <h6>Sudah ada janji dengan : {{ $data->nama_pjm }}</h6>
                                                                      </blockquote>
                                                                    <label for="inputData{{ $data->id }}" class="form-label">Pilih PJM : </label>
                                                                    <div>
                                                                        <select class="form-control" id="pilihPJM" name="pilihPJM">
                                                                            @foreach($daftarPJM as $user)
                                                                                @if($user->status != 'BUSY')
                                                                                    <option value="{{ $user->id_pjm }}" {{ $data->assigned_pjm == $user->name ? 'selected' : '' }}>{{ $user->name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>

                                                                    {{-- <input type="text" class="form-control" id="inputData{{ $data->id }}" name="inputData{{ $data->id }}"> --}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" onclick="submitForm()" class="btn btn-primary" {{ $data->assigned_pjm ? 'disabled' : '' }}>Simpan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        {{-- <div id="alertSuccess{{ $data->id }}" class="alert alert-success" style="display: none;" role="alert">
                                                            Berhasil melakukan assign!
                                                        </div> --}}
                                                            <script>
                                                                function showSuccessAlert() {
                                                                    document.getElementById('alertSuccess{{ $data->id }}').style.display= 'block';
                                                                    setTimeout(function() {
                                                                        document.getElementById('alertSuccess{{ $data->id }}').style.display = 'none';
                                                                    }, 3000); // 3000 milidetik = 3 detik
                                                                }

                                                            </script>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                            @foreach ($konsul as $data)
                                            <!-- Modal Edit -->
                                            <div class="modal fade" id="editModal{{ $data->id }}" tabindex="-1" aria-labelledby="editModalLabel{{ $data->id }}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabel{{ $data->id }}">Edit <span class="btn btn-info btn-rounded btn-fw btn-sm">No. Antrian : {{ $data->no_antrian }}</span></h5>

                                                            {{-- <button type="button" data-bs-dismiss="modal"><i class="mdi mdi-close-circle"></i></button> --}}
                                                            <button type="button" data-bs-dismiss="modal" class="btn btn-outline-secondary btn-rounded btn-icon">
                                                                <i class="mdi mdi-close"></i>
                                                              </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Form inputan bisa dimasukkan di sini -->
                                                            <form id="editForm" method="POST" action="{{ route('update.assign', ['id' => $data->id]) }}">
                                                                @csrf
                                                                @method('PUT')
                                                                <div class="mb-3">
                                                                    <label>Informasi Pemrakarsa :</label>
                                                                    <blockquote class="blockquote">
                                                                        <h6>Nama : {{ $data->nama }}</h6>
                                                                        <h6>Asal Instansi : {{ $data->asal_instansi }}</h6>
                                                                        <h6>No. HP : {{ $data->no_hp }}</h6>
                                                                        <h6>Jenis Konsultasi : {{ $data->jenis_konsultasi }}</h6>
                                                                        <h6>Deskripsi Keluhan : {{ $data->desc_keluhan }}</h6>
                                                                        <h6>Waktu Konsultasi : {{ $data->created_at }}</h6>
                                                                        <h6>Sudah ada janji dengan : {{ $data->nama_pjm }}</h6>
                                                                      </blockquote>
                                                                    <label for="inputData{{ $data->id }}" class="form-label">Pilih PJM : </label>
                                                                    <div>
                                                                        <select class="form-control" id="pilihPJM" name="pilihPJM">
                                                                        @foreach($daftarPJM as $user)
                                                                            @if($user->status != 'BUSY')
                                                                                <option value="{{ $user->id_pjm }}" {{ $data->assigned_pjm == $user->name ? 'selected' : '' }}>{{ $user->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                        </select>
                                                                    </div>
                                                                    {{-- <input type="text" class="form-control" id="inputData{{ $data->id }}" name="inputData{{ $data->id }}"> --}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" onclick="UpdateForm()" class="btn btn-primary">Simpan Perubahan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <script>
                                                            function UpdateForm() {
                                                                // Simpan perubahan pada formulir sebelum form dikirim
                                                                document.getElementById('editForm').submit();
                                                            }
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach


                                            <!-- Modal Delete -->
                                            @foreach ($konsul as $data)
                                            <div class="modal fade" id="deleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="deleteModalLabel{{ $data->id }}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="deleteModalLabel{{ $data->id }}">Delete Antrian  <span class="btn btn-info btn-rounded btn-fw btn-sm"> No. Antrian : {{ $data->no_antrian }}</span></h5>

                                                            {{-- <button type="button" data-bs-dismiss="modal"><i class="mdi mdi-close-circle"></i></button> --}}
                                                            <button type="button" data-bs-dismiss="modal" class="btn btn-outline-secondary btn-rounded btn-icon">
                                                                <i class="mdi mdi-close"></i>
                                                              </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Form inputan bisa dimasukkan di sini -->
                                                            <form id="deleteForm" action="{{ route('penugasan.destroy', $data->id) }}" method="post">
                                                                @csrf
                                                                @method('DELETE')
                                                                <div class="mb-3">
                                                                    <h6>Apakah Anda yakin ingin menghapus data ini?</h6>
                                                                    <button type="submit" class="btn btn-success btn-rounded btn-fw" onclick="deleteItem()">Ya</button>
                                                                    <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="goBack()">Tidak</button>
                                                                    {{-- <input type="text" class="form-control" id="inputData{{ $data->id }}" name="inputData{{ $data->id }}"> --}}
                                                                </div>
                                                                {{-- <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-success btn-rounded btn-fw" onclick="deleteItem()">Ya</button>
                                                                    <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="goBack()">Tidak</button>
                                                                </div> --}}
                                                            </form>
                                                        </div>

                                                        <script>
                                                            function deleteItem() {
                                                                document.getElementById('deleteForm').submit();

                                                            }

                                                            function goBack() {
                                                                window.location.href = "{{ route('penugasan.index') }}";
                                                            }

                                                            // function showDeleteNotification() {
                                                            //     var notification = document.getElementById('deleteItem');
                                                            //     notification.style.display = 'block';
                                                            //     // Sembunyikan notifikasi setelah beberapa detik (misalnya, 3 detik)
                                                            //     setTimeout(function() {
                                                            //         notification.style.display = 'none';
                                                            //     }, 3000);
                                                            // }
                                                        </script>

                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                            {{-- @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                    {{ Session::get('error') }}
                                                </div>
                                            @endif --}}

                                        </tbody>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




