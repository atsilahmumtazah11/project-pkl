@extends('layouts.pjm.app')

@section('container')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        @if(Session::has('terima'))
                            <div class="alert alert-success" role="alert">
                                {{ session('terima') }}
                            </div>
                        @endif
                        @if(Session::has('tolak'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('tolak') }}
                            </div>
                        @endif
                        <p class="card-title">Daftar Konsultasi</p>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table id="penugasan" class="display expandable-table table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nomor Antrian</th>
                                                <th>Nama Pemrakarsa</th>
                                                <th>No.HP</th>
                                                <th>Asal Instansi</th>
                                                <th>Jenis Konsultasi</th>
                                                <th>Keluhan</th>
                                                <th>Status</th>
                                                <th>Jam Konsultasi</th>
                                                <th>Assigned</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($konsul as $data)
                                                <tr>
                                                    <td>{{ $data->no_antrian }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->no_hp }}</td>
                                                    <td>{{ $data->asal_instansi }}</td>
                                                    <td>{{ $data->jenis_konsultasi }}</td>
                                                    <td>{{ $data->desc_keluhan }}</td>
                                                    <td>
                                                        @if ($data->status_transaction == 'Diterima')
                                                            <label class="badge badge-warning">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Ditolak')
                                                            <label class="badge badge-danger">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Selesai')
                                                            <label class="badge badge-success">{{ $data->status_transaction }}</label>
                                                        @elseif ($data->status_transaction == 'Menunggu')
                                                            <label class="badge badge-secondary">{{ $data->status_transaction }}</label>
                                                        @endif
                                                    </td>
                                                    <td>{{ $data->created_at }}</td>
                                                    <td>{{ $data->assigned_pjm }}</td>
                                                    <td><button type="button" class="btn btn-success btn-rounded btn-fw btn-icon" data-bs-toggle="modal" data-bs-target="#terimaModal{{ $data->id }}" {{ $data->status_transaction == 'Diterima' || $data->status_transaction == 'Ditolak' || $data->status_transaction == 'Selesai' ? 'disabled' : '' }}>
                                                        <i class="mdi mdi-check"></i></button>
                                                    </td>
                                                    <td><button type="button" class="btn btn-danger btn-rounded btn-fw btn-icon" data-bs-toggle="modal" data-bs-target="#tolakModal{{ $data->id }}" {{ $data->status_transaction == 'Diterima' || $data->status_transaction == 'Ditolak' || $data->status_transaction == 'Selesai' ? 'disabled' : '' }}>
                                                        <i class="mdi mdi-close"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                <!-- Modal Terima -->
                                                @foreach ($konsul as $data)
                                                <div class="modal fade" id="terimaModal{{ $data->id }}" tabindex="-1" aria-labelledby="terimaModalLabel{{ $data->id }}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="terimaModalLabel{{ $data->id }}">Terima Antrian  <span class="btn btn-info btn-rounded btn-fw btn-sm"> No. Antrian : {{ $data->no_antrian }}</span></h5>

                                                                {{-- <button type="button" data-bs-dismiss="modal"><i class="mdi mdi-close-circle"></i></button> --}}
                                                                <button type="button" data-bs-dismiss="modal" class="btn btn-outline-secondary btn-rounded btn-icon">
                                                                    <i class="mdi mdi-close"></i>
                                                                  </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!-- Form inputan bisa dimasukkan di sini -->
                                                                <form id="terimaPenugasan" method="POST" action="{{ route('penugasan.terima', ['id' => $data->id]) }}">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <div class="mb-3">
                                                                        <h6>Terima Konsultasi ini?</h6>
                                                                        <button type="submit" class="btn btn-success btn-rounded btn-fw" id="type-success">Ya</button>
                                                                        <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="goBack()">Batal</button>
                                                                        {{-- <input type="text" class="form-control" id="inputData{{ $data->id }}" name="inputData{{ $data->id }}"> --}}
                                                                    </div>
                                                                </form>
                                                                <script>
                                                                    function goBack() {
                                                                        window.location.href = "{{ route('penugasanPjm.index') }}";
                                                                    }
                                                                </script>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                <!-- Modal Tolak -->
                                                @foreach ($konsul as $data)
                                                <div class="modal fade" id="tolakModal{{ $data->id }}" tabindex="-1" aria-labelledby="tolakModalLabel{{ $data->id }}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="tolakModalLabel{{ $data->id }}">Tolak Antrian  <span class="btn btn-info btn-rounded btn-fw btn-sm"> No. Antrian : {{ $data->no_antrian }}</span></h5>

                                                                {{-- <button type="button" data-bs-dismiss="modal"><i class="mdi mdi-close-circle"></i></button> --}}
                                                                <button type="button" data-bs-dismiss="modal" class="btn btn-outline-secondary btn-rounded btn-icon">
                                                                    <i class="mdi mdi-close"></i>
                                                                  </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!-- Form inputan bisa dimasukkan di sini -->
                                                                <form  method="POST" action="{{ route('penugasan.tolak', ['id' => $data->id]) }}">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <div class="mb-3">
                                                                        <h6>Tolak Konsultasi ini?</h6>
                                                                        <button type="submit" class="btn btn-success btn-rounded btn-fw">Ya</button>
                                                                        <button type="button" class="btn btn-danger btn-rounded btn-fw" onclick="goBack()">Batal</button>
                                                                        {{-- <input type="text" class="form-control" id="inputData{{ $data->id }}" name="inputData{{ $data->id }}"> --}}
                                                                    </div>
                                                                </form>
                                                                <script>
                                                                    function goBack() {
                                                                        window.location.href = "{{ route('penugasanPjm.index') }}";
                                                                    }
                                                                </script>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



{{-- <table class="table">
    <thead>
      <tr>
        <th scope="col">Nama Pemrakarsa</th>
        <th scope="col">No.HP</th>
        <th scope="col">Asal Instansi</th>
        <th scope="col">Jenis Konsultasi</th>
        <th scope="col">Deskripsi Keluhan</th>
        <th scope="col">No. Antrian</th>
        <th scope="col">ID PJM</th>
        <th scope="col">Status</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data_view as $data)
            <td>{{ $data->nama }}</td>
            <td>{{ $data->no_hp }}</td>
            <td>{{ $data->asal_instansi }}</td>
            <td>{{ $data->jenis_konsultasi }}</td>
            <td>{{ $data->desc_keluhan }}</td>
            <td>{{ $data->no_antrian }}</td>
            <td>{{ $data->id_pjm }}</td>
            <td>{{ $data->status_transaction }}</tr>
        @endforeach
    </tbody>
  </table> --}}
