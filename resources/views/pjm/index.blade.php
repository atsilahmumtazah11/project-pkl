@extends('layouts.pjm.app')

@section('container')
<div class="content-wrapper">
    <div class="row">
      <div class="col-md-12 grid-margin">
        <div class="row">
          <div class="col-12 col-xl-8 mb-4 mb-xl-0">
            @if (auth()->check())
                <h3 class="font-weight-bold">Selamat datang, {{ auth()->user()->name }}!</h3>
            @else
                <h3 class="font-weight-bold">Selamat Datang, Guest</h3>
            @endif
            {{-- <h3 class="font-weight-bold">Welcome Back, {{ optional(Auth::user())->email ?? 'Guest' }}</h3> --}}
            <h6 class="font-weight-normal mb-1">Kamu mempunyai <span class="text-primary">{{ $konsultasiMenunggu }} Antrian Konsultasi</span> yang harus di Konfirmasi</h6>
            <h6 class="font-weight-normal mb-0">Kamu menolak <span class="text-danger">{{ $konsultasiDitolak }} Antrian Konsultasi!</span></h6>
          </div>
          <div class="col-12 col-xl-4">
           <div class="justify-content-end d-flex">
            <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
              <button class="btn btn-sm btn-light bg-white" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
               <i class="mdi mdi-calendar"></i> Today ({{ \Carbon\Carbon::now()->format('m/d/Y') }})
              </button>
              {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                <a class="dropdown-item" href="#">January - March</a>
                <a class="dropdown-item" href="#">March - June</a>
                <a class="dropdown-item" href="#">June - August</a>
                <a class="dropdown-item" href="#">August - November</a>
              </div> --}}
            </div>
           </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card tale-bg">
          <div class="card-people mt-auto">
            <img src="images/dashboard/people11.svg" alt="people">
            <div class="weather-info">
              <div class="d-flex">
                <div>
                  <h2 class="mb-0 font-weight-normal"><i class="icon-sun mr-2"></i>31<sup>C</sup></h2>
                </div>
                <div class="ml-2">
                  <h4 class="location font-weight-normal mb-1">Halo, Selamat Datang</h4>
                  <h6 class="font-weight-normal">{{ auth()->user()->name }}</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 grid-margin transparent">
        <div class="row">
          <div class="col-md-6 mb-4 stretch-card transparent">
            <div class="card card-tale">
              <div class="card-body">
                <p class="mb-4">Jumlah Konsultasi Perling</p>
                <p class="fs-30 mb-2">{{ $konsultasiPerling }}</p>
                {{-- <p>10.00% (30 days)</p> --}}
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4 stretch-card transparent">
            <div class="card card-dark-blue">
              <div class="card-body">
                <p class="mb-4">Jumlah Konsultasi Amdalnet</p>
                <p class="fs-30 mb-2">{{ $konsultasiAmdalnet }}</p>
                {{-- <p>22.00% (30 days)</p> --}}
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 mb-lg-0 stretch-card transparent">
            <div class="card card-light-blue">
              <div class="card-body">
                <p class="mb-4">Jumlah Konsultasi Diterima</p>
                <p class="fs-30 mb-2">{{ $konsultasiDiterima }}</p>
                {{-- <p>2.00% (30 days)</p> --}}
              </div>
            </div>
          </div>
          <div class="col-md-6 stretch-card transparent">
            <div class="card card-light-danger">
              <div class="card-body">
                <p class="mb-4">Jumlah Konsultasi Selesai</p>
                <p class="fs-30 mb-2">{{ $konsultasiSelesai }}</p>
                {{-- <p>0.22% (30 days)</p> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- <div class="row">
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <p class="card-title">Consultation Details</p>
            <p class="font-weight-500">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
            <div class="d-flex flex-wrap mb-5">
              <div class="mr-5 mt-3">
                <p class="text-muted">Order value</p>
                <h3 class="text-primary fs-30 font-weight-medium">12.3k</h3>
              </div>
              <div class="mr-5 mt-3">
                <p class="text-muted">Orders</p>
                <h3 class="text-primary fs-30 font-weight-medium">14k</h3>
              </div>
              <div class="mr-5 mt-3">
                <p class="text-muted">Users</p>
                <h3 class="text-primary fs-30 font-weight-medium">71.56%</h3>
              </div>
              <div class="mt-3">
                <p class="text-muted">Downloads</p>
                <h3 class="text-primary fs-30 font-weight-medium">34040</h3>
              </div>
            </div>
            <canvas id="order-chart"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
           <div class="d-flex justify-content-between">
            <p class="card-title">Consultation Report</p>
            <a href="#" class="text-info">View all</a>
           </div>
            <p class="font-weight-500">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
            <div id="sales-legend" class="chartjs-legend mt-4 mb-2"></div>
            <canvas id="sales-chart"></canvas>
          </div>
        </div>
      </div>
    </div> --}}

    <div class="row">
      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <p class="card-title">Daftar Konsultasi</p>
            <div class="row">
              <div class="col-12">
                <div class="table-responsive">
                  <table id="penugasan" class="display expandable-table" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Antrian</th>
                        <th>Nama Pemrakarsa</th>
                        <th>Asal Instansi</th>
                        <th>Jenis Konsultasi</th>
                        <th>Status</th>
                        <th>Jam Konsultasi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($konsul as $data)
                            <tr>
                                <td>{{ $data->no_antrian }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ $data->asal_instansi }}</td>
                                <td>{{ $data->jenis_konsultasi }}</td>
                                <td><label class="badge badge-warning">{{ $data->status_transaction }}</label></td>
                                <td>{{ $data->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection
