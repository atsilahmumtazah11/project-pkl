@extends('layouts.konsultasi.app')
@section('container')
<div class="signup-content">
    <div class="signup-form">
        <h2 class="form-title">Layanan Konsultasi</h2>

        <form method="POST" action="{{ route('createKonsultasi') }}" class="register-form" id="register-form">
            @csrf
            <div class="form-group">
                <label for="datenow"><i class="bi bi-calendar-date"></i></label>
                <input type="text" name="datenow" id="datenow" placeholder="Tanggal Hari ini : {{ \Carbon\Carbon::now()->format('m/d/Y') }}" disabled/>
            </div>
            <div class="form-group">
                <label for="nama"><i class="bi bi-person-fill"></i></label>
                <input type="text" name="nama" id="nama" placeholder="Nama Lengkap"/>
            </div>
            <div class="form-group">
                <label for="no_hp"><i class="bi bi-telephone-fill"></i></label>
                <input type="number" name="no_hp" id="no_hp" placeholder="No.HP"/>
            </div>
            <div class="form-group">
                <label for="asal_instansi"><i class="bi bi-building-fill"></i></label>
                <input type="text" name="asal_instansi" id="asal_instansi" placeholder="Asal Instansi"/>
            </div>
            <div class="form-group">
                <label for="jenis_konsultasi"><i class="bi bi-list-ul"></i></label>
                <select name="jenis_konsultasi" id="jenis_konsultasi">
                    <option disabled selected>Pilih Jenis Konsultasi</option>
                    <option value="amdalnet">Amdalnet</option>
                    <option value="perling">Perling</option>
                </select>
            </div>
            <div class="form-group">
                <label for="janji_konsultasi"><i class="bi bi-calendar-check-fill"></i></label>
                <select name="janji_konsultasi" id="janji_konsultasi">
                    <option disabled selected>Apakah sudah ada janji?</option>
                    <option value="sudah">Sudah</option>
                    <option value="belum">Belum</option>
                </select>
            </div>


            <div class="form-group" id="tambahan_input" style="display: none;">
                <label for="nama_pjm"></label>
                <input class="form-control" name="nama_pjm" type="text" id="nama_pjm" placeholder="Nama PJM : ">
            </div>

            <script>
                var janjiKonsultasi = document.getElementById('janji_konsultasi');
                var tambahanInput = document.getElementById('tambahan_input');

                janjiKonsultasi.addEventListener('change', function() {
                    if (janjiKonsultasi.value === 'sudah') {
                        tambahanInput.style.display = 'block';
                    } else {
                        tambahanInput.style.display = 'none';
                    }
                });
            </script>

            {{-- <div class="form-group">
                <label for="desc_keluhan"><i class="zmdi zmdi-lock-outline"></i></label>
                <input type="text" name="desc_keluhan" id="desc_keluhan" rows="4" placeholder="Deskripsi Keluhan"/>
            </div> --}}

            <div class="form-group">
                <label for="desc_keluhan"><i class="bi bi-card-text"></i></label>
                <textarea name="desc_keluhan" id="desc_keluhan" rows="1" placeholder="Deskripsi Keluhan"></textarea>
            </div>

            {{-- <div class="form-group">
                <label for="exampleSelectGender">Gender</label>
                  <select class="form-control" id="exampleSelectGender">
                    <option>Male</option>
                    <option>Female</option>
                  </select>
            </div> --}}
            {{-- <div class="form-group">
                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
            </div> --}}
            <div class="form-group form-button">
                <input type="submit" name="signup" id="signup" class="form-submit" value="Submit"/>
            </div>
        </form>
    </div>
    <div class="signup-image">
        <figure><img src="images/klhk2.jpg" alt="sing up image"></figure>
    </div>
</div>
@endsection
