<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasFactory, HasApiTokens;
    protected $fillable = [
        'id_pjm', 'name', 'email', 'password', 'status',
    ];

    protected $table = 'users';
    protected $primaryKey = 'id_pjm';

    public function konsultasis()
    {
        return $this->hasMany(Konsultasi::class, 'id_pjm');
    }
}
