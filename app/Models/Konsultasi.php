<?php

namespace App\Models;

use App\Models\Antrian;
use App\Events\KonsultasiSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Konsultasi extends Model
{
    use HasFactory;
    protected $table = 'konsultasis';
    protected $fillable = [
        'id',
        'nama',
        'no_hp',
        'asal_instansi',
        'jenis_konsultasi',
        'desc_keluhan',
        'no_antrian',
        'nama_pjm',
        'assigned_pjm',
        'id_pjm',
        'status_transaction',
        'pilihPJM'
    ];

    public function antrian()
    {
        return $this->belongsTo(Antrian::class);
    }

    protected $dispatchesEvents = [
        'saved' => \App\Events\KonsultasiSaved::class,
    ];

    public function assignPJM()
    {
        return $this->belongsTo(User::class, 'id_pjm');
    }

    public function rating()
    {
        return $this->belongsTo(Rating::class);
    }

};
