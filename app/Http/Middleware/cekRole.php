<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$levels)
    {
        $admin = User::join('admin', 'users.email', 'admin.email')
		->where('admin.user_type', 'admin')
		->get();

		$pjm = User::join('tuk_secretary_members', 'users.email', 'tuk_secretary_members.email')
		->join('feasibility_test_teams', 'tuk_secretary_members.id_feasibility_test_team', 'feasibility_test_teams.id')
		->where('feasibility_test_teams.authority', 'Pusat')
		->select('users.email')
		->get();

        $role = 'unregistered';

		for ($i = 0; $i < count($pjm); $i++) {
			if (Auth::user()->email == $pjm[$i]->email) {
				$role = 'Pjm';
			}
		}

        for ($i = 0; $i < count($admin); $i++) {
			if (Auth::user()->email == $admin[$i]->email) {
				$role = 'admin';
			}
		}

        if (in_array($role,$levels)) {
            return $next($request);
        }

        return redirect('/');
    }
}
