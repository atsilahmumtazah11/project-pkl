<?php

namespace App\Http\Controllers;

use App\Models\Konsultasi;
use Illuminate\Support\Facades\DB;
use App\Models\daftar_pjm;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function dashboard()
    {
        $konsul= Konsultasi::get()->all();
        return view('admin.index', compact('konsul'));
    }
    public function amdalnet()
    {
        $konsul= Konsultasi::get()->all();
        return view('admin.amdalnet', compact('konsul'));
    }

    public function perling()
    {
        $konsul= Konsultasi::get()->all();
        return view('admin.perling', compact('konsul'));
    }
    public function daftarPjm()
    {
        // Ambil semua data PJM bersama dengan konsultasi yang terkait
        $data_pjm = User::with('konsultasis')->get();

        // Loop melalui setiap PJM
        foreach ($data_pjm as $pjm) {
            // Periksa apakah PJM sedang memiliki konsultasi aktif
            $activeConsultation = $pjm->konsultasis()->where('status_transaction', 'Diterima')->exists();

            // Tentukan status berdasarkan konsultasi aktif
            $pjm->status = $activeConsultation ? 'busy' : 'idle';
        }

        // Kembalikan tampilan dengan data PJM yang telah diperbarui dengan status
        return view('admin.daftarPjm', ['data_pjm' => $data_pjm]);
    }

    public function index()
    {
        $konsul = Konsultasi::all();
        $daftarPJM = User::all(); // Ambil semua data DaftarPJM

        return view('admin.penugasan', compact('konsul', 'daftarPJM'));
    }

    public function resetAntrian(Request $request)
    {
        // Ambil nomor antrian terbaru berdasarkan jenis konsultasi
        $latestAntrian = Konsultasi::where('jenis_konsultasi', 'amdalnet')
                                   ->orWhere('jenis_konsultasi', 'perling')
                                   ->latest('created_at')
                                   ->first();

        // Jika tidak ada nomor antrian sebelumnya, set nomor antrian baru menjadi 1
        if (!$latestAntrian) {
            // Jika tidak ada nomor antrian sebelumnya, set nomor antrian baru menjadi 1
            $awalanAntrian = ($latestAntrian->jenis_konsultasi == 'amdalnet') ? 'A' : 'P';
            $nomorAntrianBaru = $awalanAntrian . ' 001';
        } else {
            // Ambil nomor antrian saja dari string
            $nomorAntrian = (int) substr($latestAntrian->no_antrian, 2);
            $nomorAntrian++;

            // Format nomor antrian dengan leading zeros
            $awalanAntrian = ($latestAntrian->jenis_konsultasi == 'amdalnet') ? 'A' : 'P';
            $nomorAntrianBaru = sprintf('%s %03d', $awalanAntrian, $nomorAntrian);
        }

        // Update nomor antrian di database
        Konsultasi::where('id', $latestAntrian->id)->update(['no_antrian' => $nomorAntrianBaru]);

        // Berikan respons untuk memberi tahu frontend bahwa reset berhasil dilakukan
        return response()->json(['message' => 'Nomor antrian telah direset.']);

        // // Ambil semua entri konsultasi
        // $konsultasis = Konsultasi::all();

        // // Reset nomor antrian untuk setiap entri
        // foreach ($konsultasis as $index => $konsultasi) {
        //     // Atur nomor antrian baru untuk setiap entri
        //     $konsultasi->update(['no_antrian' => $index + 1]);
        // }

        // // Berikan respons untuk memberi tahu frontend bahwa reset berhasil dilakukan
        // return response()->json(['message' => 'Nomor antrian telah direset.']);
    }


    public function simpanAssign(Request $request)
    {
        // Validasi input jika diperlukan
        // $request->validate([
        //     'assigned_pjm' => 'required',
        //     // ...
        // ]);

        // // Buat dan simpan data ke database
        // Konsultasi::create([
        //     'assigned_pjm' => $request->input('assigned_pjm'),
        //     // ...
        // ]);

        // return redirect('assigned');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            // Temukan data berdasarkan ID
            $konsul = Konsultasi::findOrFail($id);

            // Tampilkan konfirmasi penghapusan di session
           Session::flash('confirm_delete', 'Data berhasil dihapus: ' . $konsul->nama);

            // Hapus data
            $konsul->delete();

            return redirect()->route('penugasan.index');
        }
        catch (\Exception $e) {
            // Tangani kesalahan, misalnya data tidak ditemukan
            return redirect()->route('penugasan.index')->with('error', 'Gagal menghapus data. ' . $e->getMessage());
        }
    }
}
