<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Konsultasi;
use Illuminate\Http\Request;
use App\Models\daftar_pjm;
use App\Models\User;

class KonsultasiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $data = Konsultasi::all();
        $daftarPJM = User::all();
        return view('Admin.index', ['data_view' => $data, 'daftarPJM' => $daftarPJM]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $jenis_konsultasi = $request->jenis_konsultasi;
        $awalan_antrian = ($jenis_konsultasi == 'amdalnet') ? 'A' : 'P';
        // Ambil nomor antrian terbaru berdasarkan jenis konsultasi
        $no_antrian_terbaru = Konsultasi::where('jenis_konsultasi', $jenis_konsultasi)
                                        ->latest('created_at')->pluck('no_antrian')->first();
        // Jika tidak ada nomor antrian sebelumnya, set nomor antrian baru menjadi 1
        if (!$no_antrian_terbaru) {
            $nomor_antrian_baru = sprintf('%s 001', $awalan_antrian);
        } else {
            // Ambil nomor antrian saja dari string
            $nomor_antrian = (int) substr($no_antrian_terbaru, 2);
            $nomor_antrian++;

            // Format nomor antrian dengan leading zeros
            $nomor_antrian_baru = sprintf('%s %03d', $awalan_antrian, $nomor_antrian);
        }
        // $no_antrian = Konsultasi::latest('created_at')->select('no_antrian')->first();
        // $integerValue = intval($no_antrian->no_antrian);
        $konsul = Konsultasi::create([
        'nama'=>$request->nama,
        'no_hp'=>$request->no_hp,
        'asal_instansi'=>$request->asal_instansi,
        'jenis_konsultasi'=>$request->jenis_konsultasi,
        'desc_keluhan'=>$request->desc_keluhan,
        'no_antrian'=> $nomor_antrian_baru,
        'nama_pjm'=>$request->nama_pjm,
        'assigned_pjm'=>$request->assigned_pjm,
        // 'id_pjm'=>$request->id_pjm,
        'status_transaction'=> $request->status_transaction,
        ]);

        // $data = Konsultasi::latest('created_at')->select()->first();
        return view('konsultasi', compact('konsul'));
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $konsul = Konsultasi::find($id);
        return view('konsultasi', compact('konsul'));
        // $nama = $request->input('nama');
        // $asal_instansi = $request->input('asal_instansi');
        // $jenis_konsultasi = $request->input('jenis_konsultasi');

        // $no_antrian = $request->no_antrian;
        // $status_transaction = $request->status_transaction;

        // return view('konsultasi', [
        //     'nama' => $nama,
        //     'asal_instansi' => $asal_instansi,
        //     'jenis_konsultasi' => $jenis_konsultasi,
        //     'no_antrian' => $no_antrian,
        //     'status_transaction' => $status_transaction,
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $konsul = Konsultasi::findOrFail($id);
        return view('admin.penugasan', compact('konsul'));
    }

    public function update(Request $request, $id)
    {
        // Validasi inputan
        $request->validate([
            'pilihPJM' => 'required',
        ]);

        // Temukan data yang ingin diedit
        $konsul = Konsultasi::findOrFail($id);
        if ($konsul->status_transaction === 'Ditolak') {
            // Ubah status transaksi menjadi "Menunggu"
            $konsul->status_transaction = 'Menunggu';
        }

        $konsul->update([
            'assigned_pjm' => $request->input('pilihPJM'),
        ]);

        $pjmData = User::where('id_pjm', $request->input('pilihPJM'))->first();
        if ($pjmData) {
            $konsul->status_transaction = 'Menunggu';
            $konsul->assigned_pjm = $pjmData->name;
            $konsul->save();
            return redirect('/penugasan')->with('success', 'Data berhasil diubah');
        } else {
            // Nilai yang dipilih tidak sesuai dengan kolom lain di database
            return redirect('/penugasan')->with('error', 'Nilai yang dipilih tidak valid.');
        }

        // Redirect kembali ke halaman yang sesuai
        return redirect()->route('penugasan.index');
    }
    /**
     * Update the specified resource in storage.
     */
    public function updateAssigned(Request $request, $id)
    {
        $request->validate([
            'pilihPJM' => 'required',
        ]);

        // Ambil data berdasarkan ID
        $data_assign = Konsultasi::findOrFail($id);

        // Memeriksa apakah nilai yang dipilih sesuai dengan kolom lain di database
        $pjmData = User::where('id_pjm', $request->input('pilihPJM'))->first();

        if ($pjmData) {
            $data_assign->status_transaction = 'Menunggu';
            $data_assign->assigned_pjm = $pjmData->name;
            $data_assign->id_pjm = $pjmData->id_pjm;
            $data_assign->save();
            return redirect('/penugasan')->with('success', 'Data berhasil disimpan.');
        } else {
            // Nilai yang dipilih tidak sesuai dengan kolom lain di database
            return redirect('/penugasan')->with('error', 'Nilai yang dipilih tidak valid.');
        }

        return redirect('/penugasan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
