<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormProsesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('konsultasi');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $nama = $request->input('nama');
        $asal_instansi = $request->input('asal_instansi');
        $jenis_konsultasi = $request->input('jenis_konsultasi');

        $no_antrian = $request->no_antrian;
        $status_transaction = $request->status_transaction;


        // return $tampilan1->render().$tampilan2->render();
        return view('konsultasi', [
            'nama' => $nama,
            'asal_instansi' => $asal_instansi,
            'jenis_konsultasi' => $jenis_konsultasi,
            'no_antrian' => $no_antrian,
            'status_transaction' => $status_transaction,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
