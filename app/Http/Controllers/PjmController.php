<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Konsultasi;

use App\Models\daftar_pjm;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PjmController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function dashboard()
    {
        $konsul= Konsultasi::get()->all();
        return view('pjm.index', compact('konsul'));
    }
    public function amdalnet()
    {
        $konsul = Konsultasi::where('assigned_pjm', auth()->user()->name)->get();
        return view('pjm.amdalnetPjm', compact('konsul'));
    }
    public function perling()
    {
        $konsul = Konsultasi::where('assigned_pjm', auth()->user()->name)->get();
        return view('pjm.perlingPjm', compact('konsul'));
    }
    public function riwayatPjm()
    {
        // Identifikasi PJM yang sedang login
        $pjm = Auth::user();

        // Periksa apakah masih ada konsultasi dengan status "Diterima"
        $konsultasiDiterima = Konsultasi::where('assigned_pjm', $pjm->name)
                                        ->where('status_transaction', 'Diterima')
                                        ->exists();

        // Perbarui status PJM berdasarkan hasil pengecekan
        if ($konsultasiDiterima) {
            // Masih ada konsultasi yang sedang berlangsung, status PJM tetap "Busy"
            $pjm->status = 'BUSY';
        } else {
            // Tidak ada konsultasi yang sedang berlangsung, status PJM menjadi "Idle"
            $pjm->status = 'IDLE';
        }

        // Simpan perubahan status PJM ke database
        $pjm->save();

        $konsul = Konsultasi::where('assigned_pjm', $pjm->name)
                            ->whereIn('status_transaction', ['Diterima', 'Ditolak', 'Menunggu', 'Selesai' ])
                            ->get();
        // Ambil data konsultasi dengan status "Diterima" atau "Selesai"
        // $konsul = Konsultasi::where('assigned_pjm', $pjm->name)
        //                     ->whereIn('status_transaction', ['Diterima', 'Selesai'])
        //                     ->get();

        // Kembalikan tampilan dengan data konsultasi
        return view('pjm.riwayatPjm', compact('konsul'));
    }

    public function showByStatus($status)
    {
        $pjm = Auth::user();
        $konsul = Konsultasi::where('assigned_pjm', $pjm->name)
                            ->where('status_transaction', $status)
                            ->get();
        return view('pjm.riwayatPjm', compact('konsul'));
    }

    public function index()
    {
        // return view('pjm.penugasan', [
        //     'penugasanpjm' => Konsultasi::where('assigned_pjm', auth()->user()->id)->get()
        // ]);
        // $konsul = Konsultasi::all();
        // $daftarPJM = User::where('assigned_pjm', auth()->user()->name)->get();
        // return view('pjm.penugasan', compact('konsul', 'daftarPJM'));
        // $konsul= Konsultasi::get()->all();
        $konsul = Konsultasi::where('assigned_pjm', auth()->user()->name)->get();
        return view('pjm.penugasan', compact('konsul'));

    }

    public function terimaPenugasan(Request $request, $id)
    {
        // Temukan data konsultasi berdasarkan ID
        $konsul = Konsultasi::findOrFail($id);

        // Tandai konsultasi sebagai diterima oleh PJM
        $konsul->status_transaction = 'Diterima';

        // Simpan perubahan ke database
        $konsul->save();

        $pjm = User::findOrFail($konsul->id_pjm);

        // Ubah status PJM menjadi 'busy'
        $pjm->status = 'BUSY';

        // Simpan perubahan status PJM ke database
        $pjm->save();

        // Redirect kembali ke halaman penugasan khusus PJM
        return redirect()->route('penugasanPjm.index')->with('terima', 'Berhasil Menerima Konsultasi ini!');

    }

    public function tolakPenugasan(Request $request, $id)
    {
        // Temukan data konsultasi berdasarkan ID
        $konsul = Konsultasi::findOrFail($id);

        // Tandai konsultasi sebagai diterima oleh PJM
        $konsul->status_transaction = 'Ditolak';

        // Simpan perubahan ke database
        $konsul->save();

        // Redirect kembali ke halaman penugasan khusus PJM
        return redirect()->route('penugasanPjm.index')->with('tolak', 'Kamu Menolak Konsultasi ini!');
    }

    public function selesaiPenugasan(Request $request, $id)
    {
        // Temukan data konsultasi berdasarkan ID
        $konsul = Konsultasi::findOrFail($id);

        // Tandai konsultasi sebagai selesai
        $konsul->status_transaction = 'Selesai';

        // Simpan perubahan ke database
        $konsul->save();

        // Redirect kembali ke halaman riwayat pjm
        return redirect()->route('riwayatPjm.index')->with('selesai', 'Konsultasi telah diselesaikan!');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $id)
    {
        $konsultasi = Konsultasi::findOrFail($id);
        if ($konsultasi) {
            $idPJM = $request->input('id_pjm');
            $pjm = User::find($idPJM);

            if ($pjm) {
                // Periksa apakah PJM menerima konsultasi
                if ($request->has('terimaPenugasan')) {
                    // Jika menerima, ubah status PJM menjadi "busy"
                    $pjm->status = 'busy';
                } else {
                    // Jika tidak menerima, ubah status PJM menjadi "idle"
                    $pjm->status = 'idle';
                }
                // Simpan perubahan status PJM
                $pjm->save();
            }
        }
        // $daftar_pjm = daftar_pjm::create([
        //     'id_pjm'=>$request->id_pjm,
        //     'nama_pjm'=>$request->nama_pjm,
        //     'email'=>$request->email,
        //     'status_pjm'=>$request->status_pjm,
        //     ]);

        // return view('admin.daftarPjm', $daftar_pjm);
    }

    /**
     * Display the specified resource.
     */
    public function show($id_pjm)
    {
        // $data_pjm = data_pjm::find($id_pjm);
        // return view('admin.daftarPjm', compact('data_pjm'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
