<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use Illuminate\Http\Request;
use App\Models\Konsultasi;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function storeData(Request $request)
    {
        // Validasi input data
        $request->validate([
            'jenis_konsultasi' => 'required',
            'no_antrian' => 'required',
            'id' => 'required',
            'id_konsultasi' => 'required',

        ]);

        // Mulai transaksi database
        DB::beginTransaction();

        try {
            // Simpan data ke tabel pertama
            $dataTable_konsul = Konsultasi::create([
                'id' => $request->id,
                'jenis_konsultasi' => $request->input('jenis_konsultasi'),
                'no_antrian' => $request->no_antrian,
            ]);

            // Simpan data ke tabel kedua
            $dataTable_antrian = Antrian::create([
                'jenis_konsultasi' => $request->input('jenis_konsultasi'),
                'no_antrian' => $dataTable_konsul->no_antrian,
                'id_konsultasi' => $dataTable_konsul->id, // Menyimpan id dari tabel pertama
            ]);

            // Commit transaksi jika tidak ada masalah
            DB::commit();

            return response()->json(['message' => 'Data berhasil disimpan.']);
        } catch (\Exception $e) {
            // Rollback transaksi jika terjadi kesalahan
            DB::rollback();

            return response()->json(['message' => 'Terjadi kesalahan: ' . $e->getMessage()], 500);
        }
    }

}
