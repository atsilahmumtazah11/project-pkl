<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('login.index');
    }

    public function processLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);

        // dd($request->all());
        // $user = auth()->user();
        // dd($user);

        if (Auth::guard('admin')->attempt($credentials)) {
            $request->session()->regenerate();
            // dd(session()->all());
            return redirect('dashboard');

        }
        if (Auth::guard('web')->attempt($credentials)) {
            $request->session()->regenerate();
            // dd(Auth::check());
            // dd(session()->all());
            // $user = auth()->user();
            // dd($user);
            return redirect('dashboardPjm');

        }
        return back()->withErrors(['loginError'=> 'Login failed!']);
    }

    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
