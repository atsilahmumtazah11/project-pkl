<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KonsultasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('konsultasis')->insert([
            'nama' => 'Atsilah',
            'no_hp' => '082360742075',
            'asal_instansi' => 'pdluk',
            'jenis_konsultasi' => 'Amdalnet',
            'desc_keluhan' => 'blablablabla',
            'no_antrian' => 1,
            'id_pjm' => 1,
            'nama_pjm' => 'Aldo',
            'status_transaction' => 'selesai',
    ]);
        DB::table('konsultasis')->insert([
            'nama' => 'Dila',
            'no_hp' => '082360742075',
            'asal_instansi' => 'pdluk',
            'jenis_konsultasi' => 'Amdalnet',
            'desc_keluhan' => 'blablablabla',
            'no_antrian' => 1,
            'id_pjm' => 1,
            'nama_pjm' => null,
            'status_transaction' => 'Sedang Konsultasi',
    ]);
    }
}
