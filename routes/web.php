<?php

use App\Http\Middleware\cekRole;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PjmController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\KonsultasiController;
use App\Http\Controllers\FormProsesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RatingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('form2');
});

// Route::resource('admin', KonsultasiController::class);

// Route::post('/proses-form', [FormProsesController::class, 'store']);
// Route::resource('proses-form', FormProsesController::class);
// Route::resource('konsultasi', KonsultasiController::class);

// Route::post('/store-data', 'DataController@storeData');

// Route::get('/penugasan', [AdminController::class, 'index'])->name('penugasan.index');
// Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard.index');
// Route::get('/konsultasi-amdalnet', [AdminController::class, 'amdalnet'])->name('amdalnet.index');
// Route::get('/konsultasi-perling', [AdminController::class, 'perling'])->name('perling.index');
// Route::get('/daftar-pjm', [AdminController::class, 'daftarPjm'])->name('daftarPjm.index');
// Route::delete('/penugasan/{id}', [AdminController::class, 'destroy'])->name('penugasan.destroy');
// Route::post('/simpan-assign/{id}', [KonsultasiController::class, 'update'])->name('simpan.assign');

Route::middleware('auth:admin')->group(function () {
    Route::post('/reset-antrian', [AdminController::class, 'resetAntrian'])->name('reset-antrian');
    Route::get('/penugasan', [AdminController::class, 'index'])->name('penugasan.index');
    Route::get('/dashboard', [DashboardController::class, 'dashboardAdmin'])->name('dashboard');
    Route::get('/konsultasi-amdalnet', [AdminController::class, 'amdalnet'])->name('amdalnet.index');
    Route::get('/konsultasi-perling', [AdminController::class, 'perling'])->name('perling.index');
    Route::get('/daftar-pjm', [AdminController::class, 'daftarPjm'])->name('daftarPjm.index');
    Route::delete('/penugasan/{id}', [AdminController::class, 'destroy'])->name('penugasan.destroy');
    Route::post('/simpan-assign/{id}', [KonsultasiController::class, 'updateAssigned'])->name('simpan.assign');
    Route::get('/edit-assign/{id}/edit', [KonsultasiController::class, 'edit'])->name('edit.assign');
    Route::put('/edit-assign/{id}', [KonsultasiController::class, 'update'])->name('update.assign');
});

// Route::get('/penugasanPjm', [PjmController::class, 'index'])->name('penugasanPjm.index');
// Route::get('/dashboardPjm', [PjmController::class, 'dashboard'])->name('dashboardPjm.index');
// Route::get('/konsultasi-amdalnet-pjm', [PjmController::class, 'amdalnet'])->name('amdalnetPjm.index');
// Route::get('/konsultasi-perling-pjm', [PjmController::class, 'perling'])->name('perlingPjm.index');
// Route::get('/riwayatPjm', [PjmController::class, 'riwayatPjm'])->name('riwayatPjm.index');

Route::middleware('auth:web')->group(function () {
    Route::get('/penugasanPjm', [PjmController::class, 'index'])->name('penugasanPjm.index');
    Route::get('/dashboardPjm', [DashboardController::class, 'dashboardPjm'])->name('dashboardPjm.index');
    Route::get('/konsultasi-amdalnet-pjm', [PjmController::class, 'amdalnet'])->name('amdalnetPjm.index');
    Route::get('/konsultasi-perling-pjm', [PjmController::class, 'perling'])->name('perlingPjm.index');
    Route::get('/riwayat-pjm-semua', [PjmController::class, 'riwayatPjm'])->name('riwayatPjm.index');
    Route::get('/riwayat-pjm-{status}', [PjmController::class, 'showByStatus'])->name('riwayatPjm.showByStatus');
    Route::put('/penugasan-terima/{id}', [PjmController::class, 'terimaPenugasan'])->name('penugasan.terima');
    Route::put('/tolak-penugasan/{id}', [PjmController::class, 'tolakPenugasan'])->name('penugasan.tolak');
    Route::post('/penugasan/selesai/{id}', [PjmController::class, 'selesaiPenugasan'])->name('penugasanPjm.selesai');
    Route::get('/get-status', function() {
        $user = Auth::user();
        return response()->json(['status' => $user->status]); // Kirim status sebagai respons JSON
    });
});

Route::post('/konsultasi', [KonsultasiController::class, 'store'])->name('createKonsultasi');
Route::get('/konsultasi/{id}', [KonsultasiController::class, 'show'])->name('viewKonsultasi');

Route::post('/save-review', [RatingController::class, 'saveReview'])->name('savereview');

Route::get('/rating', [RatingController::class, 'index']);

// Route::get('/createKonsultasi', [KonsultasiController::class, 'store']);

// Route::get('/getData', 'DataController@getData');

// Route::get('/', [LoginController::class, 'index'])->name('login');
// Route::post('/login', [LoginController::class, 'authenticate']);

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login', [AuthController::class, 'processLogin']);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::get('/home', function () {
    return redirect('dashboard');
})->middleware('auth:admin');

Route::get('/home', function () {
    return redirect('dashboardPjm');
})->middleware('auth:web');


// Route::get('/logout', [LoginController::class, 'logout']);


// Route::group(['middleware' => [CekRole::class, 'login', 'cekRole:Pjm']], function(){
//     // Route::post('/konsultasi', [KonsultasiController::class, 'store'])->name('createKonsultasi');
//     // Route::get('/konsultasi/{id}', [KonsultasiController::class, 'show'])->name('viewKonsultasi');
//     // Route::get('/penugasan', [AdminController::class, 'index'])->name('penugasan.index');
//     // Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard.index');
//   });
